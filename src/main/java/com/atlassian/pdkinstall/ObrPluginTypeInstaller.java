package com.atlassian.pdkinstall;

import com.atlassian.plugin.DefaultPluginArtifactFactory;
import com.atlassian.plugin.PluginArtifactFactory;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.util.zip.FileUnzipper;
import com.atlassian.plugin.util.zip.Unzipper;
import org.apache.commons.io.FileUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.obr.RepositoryAdmin;
import org.osgi.service.obr.Requirement;
import org.osgi.service.obr.Resolver;
import org.osgi.service.obr.Resource;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.jar.JarFile;

public class ObrPluginTypeInstaller
{
    private final RepositoryAdmin repositoryAdmin;
    private final PluginController pluginController;
    private final PluginArtifactFactory pluginArtifactFactory = new DefaultPluginArtifactFactory();

    public ObrPluginTypeInstaller(PluginController pluginController, BundleContext bundleContext)
    {
        this.pluginController = pluginController;
        ServiceReference ref = bundleContext.getServiceReference(RepositoryAdmin.class.getName());
        this.repositoryAdmin = (RepositoryAdmin) bundleContext.getService(ref);
    }

    public void install(File plugin, List<String> errors) throws Exception
    {
        File obrDir = expandObrFile(plugin);
        URL repoUrl = new File(obrDir, "obr.xml").toURI().toURL();
        try
        {
            repositoryAdmin.addRepository(repoUrl);
            Resolver resolver = repositoryAdmin.resolver();
            List<File> pluginsToInstall = findPluginsToInstall(obrDir);
            for (File pluginJar : pluginsToInstall)
            {
                String bundleName = extractBundleName(pluginJar);
                Resource resource = repositoryAdmin.discoverResources("(symbolicname=" + bundleName + ")")[0];
                resolver.add(resource);
            }
            if (resolver.resolve())
            {
                installResources(resolver);
            }
            else
            {
                for (Requirement req : resolver.getUnsatisfiedRequirements())
                {
                    errors.add("Unable to resolve: " + req.getName() + " - " + req.getComment() + " filter: " + req.getFilter());
                }
            }
        }
        finally
        {
            repositoryAdmin.removeRepository(repoUrl);
            FileUtils.deleteDirectory(obrDir);
        }
    }

    private void installResources(Resolver resolver) throws URISyntaxException
    {
        for (Resource res : resolver.getRequiredResources())
        {
            pluginController.installPlugin(pluginArtifactFactory.create(res.getURL().toURI()));
        }
        for (Resource res : resolver.getAddedResources())
        {
            pluginController.installPlugin(pluginArtifactFactory.create(res.getURL().toURI()));
        }
        for (Resource res : resolver.getOptionalResources())
        {
            pluginController.installPlugin(pluginArtifactFactory.create(res.getURL().toURI()));
        }
    }

    private String extractBundleName(File pluginJar) throws IOException
    {
        JarFile jar = null;
        try
        {
            jar = new JarFile(pluginJar);
            return jar.getManifest().getMainAttributes().getValue(Constants.BUNDLE_SYMBOLICNAME);
        }
        finally
        {
            try
            {
                if (jar != null)
                {
                    jar.close();
                }
            }
            catch (IOException e)
            {
                // ignore
            }
        }

    }

    private List<File> findPluginsToInstall(File obrDir)
    {
        return Arrays.asList(obrDir.listFiles(new FilenameFilter()
        {
            public boolean accept(File dir, String name)
            {
                return name.endsWith(".jar");
            }
        }));
    }

    private File expandObrFile(File plugin) throws IOException
    {
        File dir = File.createTempFile("obr-", plugin.getName());
        dir.delete();
        dir.mkdir();

        Unzipper unzipper = new FileUnzipper(plugin, dir);
        unzipper.unzip();
        return dir;
    }

}
