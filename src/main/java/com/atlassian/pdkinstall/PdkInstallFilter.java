package com.atlassian.pdkinstall;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.io.IOUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.util.List;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipEntry;

import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginArtifactFactory;
import com.atlassian.plugin.DefaultPluginArtifactFactory;
import com.atlassian.plugin.PluginAccessor;

/**
 * Created by IntelliJ IDEA.
 * User: mrdon
 * Date: Dec 7, 2008
 * Time: 2:43:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class PdkInstallFilter implements Filter {

    private final FileItemFactory factory;
    private final PluginInstaller pluginInstaller;

    private static final Log log = LogFactory.getLog(PdkInstallFilter.class);

    public PdkInstallFilter(PluginInstaller pluginInstaller) {
        this.pluginInstaller = pluginInstaller;
        factory = new DiskFileItemFactory();
    }

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        if (!req.getMethod().equalsIgnoreCase("post"))
        {
            res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Requires post");
            return;
        }

        // Check that we have a file upload request
        File tmp = null;
        boolean isMultipart = ServletFileUpload.isMultipartContent(req);
        if (isMultipart)
        {
            tmp = extractJar(req, res, tmp);
        }
        else
        {
            tmp = buildJarFromFiles(req);
        }

        if (tmp != null)
        {
            List<String> errors = new ArrayList<String>();
            try
            {
                errors.addAll(pluginInstaller.install(tmp));
            }
            catch (Exception ex)
            {
                log.error(ex);
                errors.add(ex.getMessage());
            }

            tmp.delete();

            if (errors.isEmpty())
            {
                res.setStatus(HttpServletResponse.SC_OK);
                servletResponse.setContentType("text/plain");
                servletResponse.getWriter().println("Installed plugin " + tmp.getPath());
            }
            else
            {
                res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                servletResponse.setContentType("text/plain");
                servletResponse.getWriter().println("Unable to install plugin:");
                for (String err : errors)
                {
                    servletResponse.getWriter().println("\t - " + err);
                }
            }
            servletResponse.getWriter().close();
            return;
        }
        res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing plugin file");
    }

    private File extractJar(HttpServletRequest req, HttpServletResponse res, File tmp) throws IOException
    {
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // Parse the request
        try {
            List<FileItem> items = upload.parseRequest(req);
            for (FileItem item : items)
            {
                if (item.getFieldName().startsWith("file_") && !item.isFormField())
                {
                    tmp = File.createTempFile("plugindev-", item.getName());
                    tmp.renameTo(new File(tmp.getParentFile(), item.getName()));
                    item.write(tmp);
                }
            }
        } catch (FileUploadException e) {
            log.warn(e, e);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST, "Unable to process file upload");
        } catch (Exception e) {
            log.warn(e, e);
            res.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unable to process file upload");
        }
        return tmp;
    }

    private File buildJarFromFiles(HttpServletRequest req) throws IOException
    {
        File tmp;
        tmp = File.createTempFile("plugindev-", ".jar");
        ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(tmp));

        for (Enumeration e = req.getParameterNames(); e.hasMoreElements(); )
        {
            String name = (String) e.nextElement();
            if (name.startsWith("fileName_"))
            {
                String id = name.substring("fileName_".length());
                String fileName = req.getParameter(name);
                String fileData = req.getParameter("file_"+id);
                ZipEntry entry = new ZipEntry(fileName);
                zout.putNextEntry(entry);
                IOUtils.copy(new StringReader(fileData), zout);
            }
        }
        ZipEntry entry = new ZipEntry("META-INF/MANIFEST.MF");
        zout.putNextEntry(entry);
        IOUtils.copy(new StringReader("Manifest-Version: 1.0\n\n"), zout);
        zout.close();
        return tmp;
    }

    public void destroy() {
    }
}
